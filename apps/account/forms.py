from django import forms
import warnings
from .models import *
from django.contrib.auth import authenticate, get_user_model


class AuthenticationForm(forms.Form):
    username = forms.CharField(max_length=30)
    password = forms.CharField(label="Password", widget=forms.PasswordInput)
    error_messages = {
        'invalid_login': u'Please enter a correct %(username)s and password.Note that both fields may be case-sensitive.',
        'inactive': u'This account is inactive',
    }

    def __init__(self, request=None, *args, **kwargs):
        """
        The 'request' parameter is set for custom auth use by subclasses.
        The form data comes in via the standard 'data' kwarg.
        """
        self.request = request
        self.user_cache = None
        super(AuthenticationForm, self).__init__(*args, **kwargs)
        UserModel = get_user_model()

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username and password:
            self.user_cache = authenticate(username=username,
                                           password=password)
            if self.user_cache is None:
                self.add_error(None,
                               'Por favor, introduzca usuario y password correctos.')
        return self.cleaned_data

    def get_user_id(self):
        if self.user_cache:
            return self.user_cache.id
        return None

    def get_user(self):
        return self.user_cache


class CreateUserForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ('email', 'password', 'username')

    def clean_password(self):
        if len(self.cleaned_data.get('password')) < 6:
            self.add_error('password', 'Mínimo 6 caracteres.')
        else:
            return self.cleaned_data.get('password')

    def clean_email(self):  # check if username dos not exist before

        if get_user_model().objects.filter(email=self.cleaned_data['email']).exists():
            self.add_error('email', 'El email ya ha sido registrado.')
        else:
            return self.cleaned_data['email']

    def save(self, commit=True):
        user = get_user_model().objects.create(
            email=self.cleaned_data.get('email'),
            username=self.cleaned_data.get('username'),
        )

        user.set_password(self.cleaned_data.get('password'))
        user.save()
        return user


