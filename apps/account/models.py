from django.db import models
from django.contrib.auth import get_user_model
from django.db import models

user_model = get_user_model()
# Create your models here.

class CoinValues(models.TextChoices):
    FIVE_CENT = '0.05', 'Cinco Centimos'
    TEN_CENT = '0.10', 'Diez Centimos'
    TWENTY_CENT = '0.20', 'Veinte Centimos'
    FIFTY_CENT = '0.50', 'Cincuenta Centimos'
    ONE = '1.00', 'Un Sol'
    TWO = '2.00', 'Dos Soles'
    FIVE = '5.00', 'Cinco Soles'


class UserMoney(models.Model):
    user = models.ForeignKey(user_model, on_delete=models.PROTECT)
    coin_decimal_value = models.DecimalField(max_digits=6, decimal_places=2)
    coin_value = models.CharField(choices=CoinValues.choices, max_length=6)
    quantity = models.IntegerField()

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        self.coin_decimal_value = float(self.coin_value)
        super().save(force_insert, force_update, using, update_fields)

    class Meta:
        unique_together = (("user", "coin_value"),)


