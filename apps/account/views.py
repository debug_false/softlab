from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import logout, get_user_model, login
from django.contrib import auth, messages
from django.views.generic import CreateView, View, UpdateView, ListView, DetailView, TemplateView
from django.views.generic.edit import ModelFormMixin
from django.urls import reverse_lazy
from braces.views import LoginRequiredMixin, AnonymousRequiredMixin
from class_based_auth_views.views import LoginView

from .models import *
from .forms import *


class Login(AnonymousRequiredMixin, LoginView):
    template_name = 'login.html'
    form_class = AuthenticationForm
    authenticated_redirect_url = reverse_lazy('machine:dashboard')

    def get_success_url(self):
        return reverse_lazy('machine:dashboard')


class Register(AnonymousRequiredMixin, CreateView):
    model = get_user_model()
    form_class = CreateUserForm
    template_name = 'register.html'
    success_url = reverse_lazy('machine:dashboard')

    def form_valid(self, form):
        self.object = form.save()
        self.object.backend = 'django.contrib.auth.backends.ModelBackend'
        login(self.request, self.object)
        return super(ModelFormMixin, self).form_valid(form)


class LogoutView(View):
    def get(self, *args, **kwargs):
        if self.request.user.is_authenticated:
            auth.logout(self.request)
        return redirect(reverse_lazy('account:login'))
