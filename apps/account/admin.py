import random

from django.contrib import admin

from .models import UserMoney


class UserMoneyAdmin(admin.ModelAdmin):
    actions = []
    list_display = ['user', 'coin_value', 'quantity', 'coin_decimal_value']
    list_per_page = 50

    search_fields = ['user__username']

    list_filter = ['coin_value', ]

    readonly_fields = 'coin_decimal_value',


admin.site.register(UserMoney, UserMoneyAdmin)