from django import forms
import warnings
from .models import *
from ..account.models import UserMoney, CoinValues
from django.contrib.auth import authenticate, get_user_model
from .dinamic_coin_change import getchange


class DashboardForm(forms.Form):
    space = forms.ChoiceField(choices=SpaceChoices.choices,
                              widget=forms.Select(attrs={'class': 'form-control'}))
    five_cent = forms.IntegerField(initial=0)
    ten_cent = forms.IntegerField(initial=0)
    twenty_cent = forms.IntegerField(initial=0)
    fifty_cent = forms.IntegerField(initial=0)
    one = forms.IntegerField(initial=0)
    two = forms.IntegerField(initial=0)
    five = forms.IntegerField(initial=0)

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(DashboardForm, self).__init__(*args, **kwargs)

    def clean_space(self):
        stock = Stock.objects.get(space=self.cleaned_data.get('space'))
        if stock.quantity <= 0:
            self.add_error('space', 'No hay stock de este producto por favor elija otro.')
        else:
            return self.cleaned_data.get('space')

    def clean_five_cent(self):
        if self.cleaned_data.get('five_cent') < 0:
            self.add_error('five_cent', 'No puede ser menor de 0.')
        elif self.cleaned_data.get('five_cent') > self.validate_stock_user(CoinValues.FIVE_CENT):
            self.add_error('five_cent', 'No tienes suficientes monedas tienes que recargar')
        else:
            return self.cleaned_data.get('five_cent')

    def clean_ten_cent(self):
        if self.cleaned_data.get('ten_cent') < 0:
            self.add_error('ten_cent', 'No puede ser menor de 0.')
        elif self.cleaned_data.get('ten_cent') > self.validate_stock_user(CoinValues.TEN_CENT):
            self.add_error('ten_cent', 'No tienes suficientes monedas tienes que recargar')
        else:
            return self.cleaned_data.get('ten_cent')

    def clean_twenty_cent(self):
        if self.cleaned_data.get('twenty_cent') < 0:
            self.add_error('twenty_cent', 'No puede ser menor de 0.')
        elif self.cleaned_data.get('twenty_cent') > self.validate_stock_user(CoinValues.TWENTY_CENT):
            self.add_error('twenty_cent', 'No tienes suficientes monedas tienes que recargar')
        else:
            return self.cleaned_data.get('twenty_cent')

    def clean_fifty_cent(self):
        if self.cleaned_data.get('fifty_cent') < 0:
            self.add_error('fifty_cent', 'No puede ser menor de 0.')
        elif self.cleaned_data.get('fifty_cent') > self.validate_stock_user(CoinValues.FIFTY_CENT):
            self.add_error('fifty_cent', 'No tienes suficientes monedas tienes que recargar')
        else:
            return self.cleaned_data.get('fifty_cent')

    def clean_one(self):
        if self.cleaned_data.get('one') < 0:
            self.add_error('one', 'No puede ser menor de 0.')
        elif self.cleaned_data.get('one') > self.validate_stock_user(CoinValues.ONE):
            self.add_error('one', 'No tienes suficientes monedas tienes que recargar')
        else:
            return self.cleaned_data.get('one')

    def clean_two(self):
        if self.cleaned_data.get('two') < 0:
            self.add_error('two', 'No puede ser menor de 0.')
        elif self.cleaned_data.get('two') > self.validate_stock_user(CoinValues.TWO):
            self.add_error('two', 'No tienes suficientes monedas tienes que recargar')
        else:
            return self.cleaned_data.get('two')

    def clean_five(self):
        if self.cleaned_data.get('five') < 0:
            self.add_error('five', 'No puede ser menor de 0.')
        elif self.cleaned_data.get('five') > self.validate_stock_user(CoinValues.FIVE):
            self.add_error('five', 'No tienes suficientes monedas tienes que recargar')
        else:
            return self.cleaned_data.get('five')

    def validate_stock_user(self, coin_value):
        user_money = UserMoney.objects.filter(user=self.user, coin_value=coin_value)
        if user_money:
            return user_money.first().quantity
        else:
            return 0

    def clean(self):
        space = self.cleaned_data.get('space')
        five_cent = self.cleaned_data.get('five_cent')
        ten_cent = self.cleaned_data.get('ten_cent')
        twenty_cent = self.cleaned_data.get('twenty_cent')
        fifty_cent = self.cleaned_data.get('fifty_cent')
        one = self.cleaned_data.get('one')
        two = self.cleaned_data.get('two')
        five = self.cleaned_data.get('five')

        if space and five_cent and ten_cent and twenty_cent and fifty_cent and one and two and five:
            stock = Stock.objects.filter(space=space).first()
            total = (five_cent * float(CoinValues.FIVE_CENT)) + (ten_cent * float(CoinValues.TEN_CENT)) + (
                    twenty_cent * float(CoinValues.TWENTY_CENT)) + (fifty_cent * float(CoinValues.FIFTY_CENT)) + (
                            one * float(CoinValues.ONE)) + (two * float(CoinValues.TWO)) + (
                            five * float(CoinValues.FIVE))
            if not stock:
                forms.ValidationError('No hay stock de este producto')
            else:
                if stock.product.price > total:
                    self.add_error('space', 'Dinero Insuficiente por favor agregue mas credito.')
        return self.cleaned_data
