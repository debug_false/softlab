from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import logout, get_user_model, login
from django.contrib import auth, messages
from django.views.generic import CreateView, View, UpdateView, ListView, DetailView, TemplateView, FormView
from django.views.generic.edit import ModelFormMixin
from django.urls import reverse_lazy
from braces.views import LoginRequiredMixin, AnonymousRequiredMixin
from class_based_auth_views.views import LoginView
from django.contrib import messages
from .models import *
from .forms import *

from ..account.models import UserMoney


class Dashboard(LoginRequiredMixin, FormView):
    login_url = 'account:login'
    template_name = 'dashboard.html'
    form_class = DashboardForm
    success_url = reverse_lazy('machine:dashboard')

    def get_form_kwargs(self):
        form = super().get_form_kwargs()
        form['user'] = self.request.user
        return form

    def get_context_data(self, **kwargs):
        context = super(Dashboard, self).get_context_data(**kwargs)
        context['stocks'] = Stock.objects.all()
        context['usercoins'] = UserMoney.objects.filter(user=self.request.user)
        context['user'] = self.request.user
        return context

    def form_valid(self, form):
        total = 0
        message_change = ' '
        message_payment = ' '
        ret = super(Dashboard, self).form_valid(form)
        list_values = [form.cleaned_data.get('five_cent'), form.cleaned_data.get('ten_cent'),
                       form.cleaned_data.get('twenty_cent'), form.cleaned_data.get('fifty_cent'),
                       form.cleaned_data.get('one'), form.cleaned_data.get('two'), form.cleaned_data.get('five')]
        space = form.cleaned_data.get('space')
        for i in range(0, len(list_values)):
            if list_values[i] > 0:
                total += list_values[i] * float(CoinValues.values[i])
        stock = Stock.objects.filter(space=space).first()
        coins = [i.coins_json for i in MachineMoney.objects.all()]
        change = total - float(stock.product.price)
        result = getchange(coins, int(round(change, 2) * 100))
        if result is not None:
            purchase = Purchase.objects.create(user=self.request.user, product=stock.product)
            for i in range(0, len(list_values)):
                if list_values[i] > 0:
                    user = UserMoney.objects.filter(user=self.request.user,
                                                    coin_decimal_value=float(CoinValues.values[i])).first()
                    if not user:
                        UserMoney.objects.create(user=self.request.user,
                                                 coin_value="{:.2f}".format(CoinValues.values[i]),
                                                 quantity=0)
                    user.quantity = user.quantity - list_values[i]
                    user.save()
                    machine = MachineMoney.objects.filter(coin_decimal_value=float(CoinValues.values[i])).first()
                    machine.quantity = machine.quantity + list_values[i]
                    machine.save()
                    message_payment = message_payment + "{} $: {} monedas".format(float(CoinValues.values[i]),
                                                                                  list_values[i])
            purchase.history_payment = "Pago la cantidad de " + message_payment

            for i in result:
                machine = MachineMoney.objects.filter(coin_decimal_value=float(i['value']) / 100).first()
                machine.quantity -= i['count']
                machine.save()
                user = UserMoney.objects.filter(user=self.request.user,
                                                coin_decimal_value=float(i['value']) / 100).first()
                if not user:
                    user = UserMoney.objects.create(user=self.request.user,
                                                    coin_value="{:.2f}".format(i['value'] / 100),
                                                    quantity=0)
                user.quantity += i['count']
                user.save()
                message_change = message_change + "{} $ : {} monedas".format(float(i['value']) / 100, i['count'])
            stock.quantity -= 1
            stock.save()
            if len(message_change) < 2:
                message_change = "0 soles"
            messages.add_message(self.request, messages.SUCCESS, "Su cambio es de " + message_change)
            purchase.history_change = "Su cambio es de " + message_change
            purchase.save()
        else:
            messages.add_message(self.request, messages.SUCCESS, 'La maquina no tiene Cambio')
        return ret

