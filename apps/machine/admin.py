import random

from django.contrib import admin

from .models import Product, Stock, SpaceChoices, MachineMoney, Purchase


class MachineMoneyAdmin(admin.ModelAdmin):
    actions = []
    list_display = ['coin_value', 'quantity', 'coin_decimal_value']
    list_per_page = 50

    list_filter = ['coin_value', ]

    readonly_fields = 'coin_decimal_value',


class ProductAdmin(admin.ModelAdmin):
    actions = []
    list_display = ['name', 'price', ]
    list_per_page = 50

    search_fields = ['name']

    list_filter = ['price', ]


admin.site.register(Product, ProductAdmin)


def refill(modeladmin, request, queryset):
    products_id_list = list(Product.objects.values_list('id', flat=True))
    for choice in SpaceChoices:
        stock = Stock.objects.filter(space=choice).first()
        if stock:
            stock.quantity = 10
            stock.save()
        else:
            product_random_id = random.choice(products_id_list)
            stock = Stock.objects.create(space=choice, quantity=10, product_id=product_random_id)


class StockAdmin(admin.ModelAdmin):
    actions = [refill, ]
    list_display = ['space', 'product', 'quantity']
    list_per_page = 50

    search_fields = ['product__name']


class PurchaseAdmin(admin.ModelAdmin):
    list_display = ['code', 'user', 'product', ]
    list_per_page = 50
    search_fields = ['code', ]
    list_filter = ['code', ]


admin.site.register(Purchase, PurchaseAdmin)
admin.site.register(Stock, StockAdmin)
admin.site.register(MachineMoney, MachineMoneyAdmin)
