from django.db import models
from django.contrib.auth import get_user_model
from django.db import models
from ..account.models import CoinValues
import collections
from django.utils.crypto import get_random_string

user_model = get_user_model()


class MachineMoney(models.Model):
    coin_decimal_value = models.DecimalField(max_digits=6, decimal_places=2)
    coin_value = models.CharField(choices=CoinValues.choices, max_length=6, unique=True)
    quantity = models.IntegerField()

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        self.coin_decimal_value = float(self.coin_value)
        super().save(force_insert, force_update, using, update_fields)

    @property
    def coins_json(self):
        coins_json = {"value": int(self.coin_decimal_value * 100), "count": self.quantity}
        return coins_json


class Product(models.Model):
    name = models.CharField(max_length=150, unique=True)
    price = models.DecimalField(max_digits=6, decimal_places=2)

    def __str__(self):
        return self.name


class SpaceChoices(models.TextChoices):
    AA = 'AA', 'AA'
    AB = 'AB', 'AB'
    AC = 'AC', 'AC'
    BA = 'BA', 'BA'
    BB = 'BB', 'BB'
    BC = 'BC', 'BC'
    CA = 'CA', 'CA'
    CB = 'CB', 'CB'
    CC = 'CC', 'CC'


class Stock(models.Model):
    space = models.CharField(max_length=2, choices=SpaceChoices.choices, unique=True)
    product = models.ForeignKey(Product, on_delete=models.PROTECT)
    quantity = models.IntegerField()


class Purchase(models.Model):
    user = models.ForeignKey(user_model, on_delete=models.PROTECT)
    product = models.ForeignKey(Product, on_delete=models.PROTECT)
    history_payment = models.TextField()
    history_change = models.TextField()
    code = models.CharField(max_length=10, blank=True, null=True)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.code:
            self.code = get_random_string(7)
        super().save(force_insert, force_update, using, update_fields)
