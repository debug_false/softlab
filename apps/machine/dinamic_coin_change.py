def getchange(coins, amount):
    # Algoritmo basado en programacion dinamica para tomar la mejor opcion a la hora de devolver el cambio
    minCount = None

    def recurse(amount, coinIndex, coinCount):
        nonlocal minCount
        if amount == 0:
            if minCount == None or coinCount < minCount:
                minCount = coinCount
                return []  # exitoso
            return None  # no optimo
        if coinIndex >= len(coins):
            return None  # fallido
        bestChange = None
        coin = coins[coinIndex]
        # Comienzo tomando la mayor cantidad de esta moneda
        cantake = min(amount // coin["value"], coin["count"])
        # Reducir el numero de esta moneda a 0
        for count in range(cantake, -1, -1):
            # Recurrir sacando esta moneda como posible opcion
            change = recurse(amount - coin["value"] * count, coinIndex + 1,
                             coinCount + count)
            # Buscamos la mejor solucion
            if change != None:
                if count:
                    change.append({"value": coin["value"], "count": count})
                bestChange = change  # Registrar como el mejor hasta ahora
        return bestChange

    return recurse(amount, 0, 0)
